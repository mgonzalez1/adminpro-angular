import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';

import { IncrementadorComponent } from './incrementador/incrementador.component';
import { DoughnutChartComponent } from './doughnut-chart/doughnut-chart.component';
import { ModalUploadComponent } from './modal-upload/modal-upload.component';
import { PipesModule } from '../pipes/pipes.module';

@NgModule({
    declarations: [
        IncrementadorComponent,
        DoughnutChartComponent,
        ModalUploadComponent
    ],
    exports: [
        IncrementadorComponent,
        DoughnutChartComponent,
        ModalUploadComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        ChartsModule,
        PipesModule
    ]
})

export class ComponentsModule { }
