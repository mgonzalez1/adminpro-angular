import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-doughnut-chart',
  templateUrl: './doughnut-chart.component.html',
  styleUrls: ['./doughnut-chart.component.css']
})
export class DoughnutChartComponent implements OnInit {

  @Input('grafico-labels') doughnutChartLabels: string[] = [];
  @Input('grafico-data') doughnutChartData: number[] = [];
  @Input('grafico-type') doughnutChartType: string = '';

  constructor() { }

  ngOnInit() {
  }

}
