import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Hospital } from '../../models/hospital.model';
import { URL_SERVICIOS } from '../../config/config';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

// import * as _swal from 'sweetalert';
// import { SweetAlert } from 'sweetalert/typings/core';

import { SubirArchivoService } from '../subir-archivo/subir-archivo.service';
import { UsuarioService } from '../usuario/usuario.service';

// const swal: SweetAlert = _swal as any;

@Injectable()
export class HospitalService {

  constructor(
    public http: HttpClient,
    public _subirArchivoService: SubirArchivoService,
    public _usuarioService: UsuarioService,
  ) { }

  cargarHospitales(desde: number = 0, limit: number = 5) {
    let url = URL_SERVICIOS + '/hospital?desde=' + desde + '&limit=' + limit;

    return this.http.get(url);
  }

  obtenerHospital(id: string) {
    let url = URL_SERVICIOS + '/hospital/' + id;

    return this.http.get(url).map((resp: any) => {
      return resp.hospital;
    });
  }

  borrarHospital(id: string) {
    let url = URL_SERVICIOS + '/hospital/' + id;
    url += '?token=' + this._usuarioService.token;

    return this.http.delete(url).map((resp: any) => {
      swal({
        title: 'Hospital borrado',
        text: 'El hospital a sido eliminado correctamente',
        icon: 'success'
      }).then((ok) => {
        return true;
      });
    });
  }

  crearHospital(	nombre:	string	) {
    let url = URL_SERVICIOS + '/hospital?token=' + this._usuarioService.token;
    let hospital = new Hospital( nombre );
    return this.http.post(url, hospital)
      .map((resp: any) => {
        return resp.hospital;
      });
  }

  buscarHospital(termino: string) {
    let url = URL_SERVICIOS + '/busqueda/coleccion/hospitales/' + termino;

    return this.http.get(url).map((resp: any) => {
      return resp.hospitales;
    });
  }

  actualizarHospital(hospital: Hospital) {
    let url = URL_SERVICIOS + '/hospital/' + hospital._id;
    url += '?token=' + this._usuarioService.token;

    return this.http.put(url, hospital).map((resp: any) => {
      swal({
        title: 'Hospital actualizado',
        text: hospital.nombre,
        icon: 'success'
      }).then((ok) => {
        return resp.hospital;
      });
    });
  }

}
