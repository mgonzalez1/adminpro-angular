import { Injectable, isObservable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { Usuario } from '../../models/usuario.model';
import { URL_SERVICIOS } from '../../config/config';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';

// import * as _swal from 'sweetalert';
// import { SweetAlert } from 'sweetalert/typings/core';

import { SubirArchivoService } from '../subir-archivo/subir-archivo.service';

// const swal: SweetAlert = _swal as any;

@Injectable()
export class UsuarioService {

  usuario: Usuario;
  token: string;
  menu: any[] = [];

  constructor(
    public http: HttpClient,
    public router: Router,
    public _subirArchivoService: SubirArchivoService
  ) {
    this.cargarStorage();
  }

  estaLogueado() {

    return ( this.token.length > 5 ) ? true : false;

  }

  cargarStorage() {

    if (localStorage.getItem('token')) {
      this.token = localStorage.getItem('token');
      this.usuario = JSON.parse(localStorage.getItem('usuario'));
      this.menu = JSON.parse(localStorage.getItem('menu'));
    } else {
      this.token = '';
      this.usuario = null;
      this.menu = [];
    }

  }

  guardarStorage( id: string, token: string, usuario: Usuario, menu: any ) {

    localStorage.setItem('id', id);
    localStorage.setItem('token', token);
    localStorage.setItem('usuario', JSON.stringify(usuario));
    localStorage.setItem('menu', JSON.stringify(menu));

    this.usuario = usuario;
    this.token = token;
    this.menu = menu;

  }

  logout() {
    this.usuario = null;
    this.token = '';

    localStorage.removeItem('id');
    localStorage.removeItem('token');
    localStorage.removeItem('usuario');
    localStorage.removeItem('menu');

    this.router.navigate(['/login']);

  }

  login( usuario: Usuario, recordar: boolean = false ) {

    if ( recordar ) {
      localStorage.setItem('email', usuario.email);
    } else {
      localStorage.removeItem('email');
    }

    const url = URL_SERVICIOS + '/login';

    return this.http.post(url, usuario)
                .map( ( resp: any ) => {

                  this.guardarStorage(resp.id, resp.token, resp.usuario, resp.menu);

                  return true;
                })
                .catch(err => {
                  swal('Error en el login', err.error.mensaje, 'error');
                  return Observable.throw(err);
                });
  }

  loginGoogle(token: string) {

    const url = URL_SERVICIOS + '/login/google';

    return this.http.post(url, { token })
      .map((resp: any) => {
        console.log(resp);

        this.guardarStorage( resp.id, resp.token, resp.usuario, resp.menu );

        return true;
      });
  }

  crearUsuario(usuario: Usuario) {

    const url = URL_SERVICIOS + '/usuario';

    return this.http.post(url, usuario)
                .map( (resp: any) => {

                  swal({
                    title: 'Usuario creado',
                    text: usuario.email,
                    icon: 'success'
                  }).then((ok) => {
                    return resp.usuario;
                  });

                })
                .catch(err => {
                  swal(err.error.mensaje, err.error.errors.message, 'error');
                  return Observable.throw(err);
                });

  }

  actualizarUsuario(usuario: Usuario) {

    let url = URL_SERVICIOS + '/usuario/' + usuario._id;
    url += '?token=' + this.token;

    return this.http.put(url, usuario).map((resp: any) => {
      if (usuario._id === this.usuario._id) {
        const usuarioDB: Usuario = resp.usuario;
        this.guardarStorage(usuarioDB._id, this.token, usuarioDB, this.menu);
      }
      swal({
        title: 'Usuario actualizado',
        text: usuario.nombre,
        icon: 'success'
      }).then((ok) => {
        return true;
      });
    })
    .catch(err => {
      swal(err.error.mensaje, err.error.errors.message, 'error');
      return Observable.throw(err);
    });

  }

  cambiarImagen(archivo: File, id: string ) {

    this._subirArchivoService.subirArchivo( archivo, 'usuarios', id)
            .then((resp: any) => {

              this.usuario.img = resp.usuario.img;
              swal({
                title: 'Imagen actualizada',
                text: this.usuario.nombre,
                icon: 'success'
              }).then((ok) => {
                this.guardarStorage(id, this.token, this.usuario, this.menu);
              });

            })
            .catch(resp => console.log(resp));

  }

  cargarUsuarios(desde: number = 0) {
    const url = URL_SERVICIOS + '/usuario?desde=' + desde;

    return this.http.get(url);
  }

  buscarUsuario(termino: string) {
    const url = URL_SERVICIOS + '/busqueda/coleccion/usuarios/' + termino;

    return this.http.get(url).map((resp: any) => {
      return resp.usuarios;
    });
  }

  borrarUsuario(id: string) {
    let url = URL_SERVICIOS + '/usuario/' + id;
    url += '?token=' + this.token;

    return this.http.delete(url).map((resp: any) => {
      swal({
        title: 'Usuario borrado',
        text: 'El usuario a sido eliminado correctamente',
        icon: 'success'
      }).then((ok) => {
        return true;
      });
    });
  }

}
