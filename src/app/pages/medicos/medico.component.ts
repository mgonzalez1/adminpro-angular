import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { MedicoService, HospitalService, ModalUploadService } from '../../services/service.index';
import { Medico } from '../../models/medico.model';
import { Hospital } from '../../models/hospital.model';

@Component({
  selector: 'app-medico',
  templateUrl: './medico.component.html',
  styles: []
})
export class MedicoComponent implements OnInit {

  hospitales: Hospital[] = [];
  medico: Medico = new Medico('', '', '', '');
  hospital: Hospital = new Hospital('');  
  // col: string = 'col-12';

  constructor(
    public router: Router,
    public activateRoute: ActivatedRoute,
    public _medicoService: MedicoService,
    public _hospitalService: HospitalService,
    public _modalUploadService: ModalUploadService
  ) {
    activateRoute.params.subscribe(params => {
      let id = params['id'];

      if (id !== 'nuevo') {
        this.cargarMedico(id);
      }
    });
  }

  ngOnInit() {
    this._hospitalService.cargarHospitales(0, -1).
          subscribe((resp: any) => this.hospitales = resp.hospitales);

    this._modalUploadService.notificacion
          .subscribe((resp: any) => {
            this.medico.img = resp.medico.img;
          });
  }

  cargarMedico( id: string) {
    this._medicoService.cargarMedico(id)
          .subscribe((medico: any) => {
            this.medico = medico;
            this.hospital = medico.hospital;

            this.medico.hospital = this.hospital._id;
          });
  }

  guardarMedico(f: NgForm) {
    if(f.invalid) {
      return;
    }

    this._medicoService.guardarMedico(this.medico)
          .subscribe((medico) => {
            if (medico) {
              this.medico._id = medico._id;
              // this.col = 'col-6';
              this.router.navigate(['/medico', medico._id]);
            }
          });
  }

  cambioHospital(id: string) {
    this._hospitalService.obtenerHospital(id)
          .subscribe((hospital) => {
            this.hospital = hospital;
          });
  }

  cambiarFoto() {
    this._modalUploadService.mostrarModal('medicos', this.medico._id);
  }

}
