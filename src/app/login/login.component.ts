import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

import { UsuarioService } from '../services/service.index';
import { Usuario } from '../models/usuario.model';

declare function init_plugins();
declare const gapi: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  recuerda: boolean = false;
  email: string;
  dashboardPage: string = 'dashboard';

  auth2: any;

  constructor(
    public router: Router,
    public _usuarioService: UsuarioService
  ) { }

  ngOnInit() {
    init_plugins();
    this.googleInit();

    this.email = localStorage.getItem('email') || '';

    if (this.email.length) {
      this.recuerda = true;
    }

  }

  googleInit() {
    gapi.load('auth2', () => {
      this.auth2 = gapi.auth2.init({
        client_id: '613736417885-iv35js32cjpuaqba7s19ga886hu571co.apps.googleusercontent.com',
        cookiepolicy: 'single_host_origin',
        scope: 'profile email'
      });

      this.attachSignin( document.getElementById('btnGoogle') );
    });
  }

  attachSignin ( element ) {

    this.auth2.attachClickHandler(element, {}, googleUser => {

      // let profile = googleUser.getBasicProfile();

      let token = googleUser.getAuthResponse().id_token;

      this._usuarioService.loginGoogle( token )
        .subscribe(correcto => window.location.href = '#/' + this.dashboardPage);
    });

  }

  ingresar( forma: NgForm ) {

    let usuario = new Usuario(null, forma.value.email, forma.value.password );

    console.log(forma.value);

    this._usuarioService.login( usuario, forma.value.recuerdame)
               .subscribe(correcto => window.location.href = '#/' + this.dashboardPage);

  }

}
